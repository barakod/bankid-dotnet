﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Barakod.Authentication.BankID.Extensions;
using Barakod.Authentication.BankID.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Barakod.Authentication.BankID.Examples.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var rootCertificateRelativePath = Configuration["BANKID_ROOT_CERT_RELATIVE_PATH"];
            var privateCertificateRelativePath = Configuration["BANKID_PRIVATE_CERT_RELATIVE_PATH"];
            var privateCertificatePassword = Configuration["BANKID_PRIVATE_CERT_PASSWORD"];
            var isProductionString = Configuration["BANKID_ENVIRONMENT_IS_PROD"];

            var environment = isProductionString == null || isProductionString.ToLower() != "production"
                ? BankIDApiEnvironment.TestV322
                : BankIDApiEnvironment.ProductionV322;

            services.AddLogging(builder => builder
                .SetMinimumLevel(LogLevel.Information)
                .AddConsole());

            services.AddBankId(environment,
                               rootCertificateRelativePath,
                               privateCertificateRelativePath,
                               privateCertificatePassword);


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

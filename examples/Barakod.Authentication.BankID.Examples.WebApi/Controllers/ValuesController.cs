﻿using Barakod.Authentication.BankID.Models.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID.Examples.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private IBankIDService _bankIDservice;

        public ValuesController(IBankIDService bankIDService)
        {
            _bankIDservice = bankIDService;
        }

        [HttpGet]
        public async Task<IActionResult> StartAutostartAuthRequest()
        {
            var request = new AuthRequest { EndUserIp = "192.168.0.1" };

            return Ok(await _bankIDservice.BeginAuthRequest(request));
        }

        [HttpGet("personal/{personal}")]
        public async Task<IActionResult> StartPersonalAuthRequest(string personal)
        {
            var request = new AuthRequest { EndUserIp = "192.168.0.1", PersonalNumber = personal };

            return Ok(await _bankIDservice.BeginAuthRequest(request));
        }

        [HttpGet("collect/{orderRef}")]
        public async Task<IActionResult> CollectRequestStatus(string orderRef)
        {
            var request = new CollectAuthRequest { OrderRef = orderRef };

            return Ok(await _bankIDservice.CollectRequestStatus(request));
        }
    }
}

﻿using Barakod.Authentication.BankID.Models.Dtos;
using Barakod.Authentication.BankID.Models.ValueObjects;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Barakod.Authentication.BankID.Exceptions
{
    public class BankIDException : Exception
    {
        public Error Error { get; set; }
        public BankIDException(Error error) : base(error.Details)
        {
        }

        public BankIDException(Error error, Exception innerException) : base(error.Details, innerException)
        {
        }


    }
}

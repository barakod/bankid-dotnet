﻿using Barakod.Authentication.BankID.Models;
using Barakod.Authentication.BankID.Models.Dtos;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Cryptography.X509Certificates;

namespace Barakod.Authentication.BankID.Contracts
{
    public interface IBankIDHttpClientServiceConfiguration
    {
        Uri BaseUrl { get; }
        X509Certificate2 RootCert { get; }
        X509Certificate2Collection PrivateCert { get; }
        BankIDApiEnvironment Environment { get; }
    }
}
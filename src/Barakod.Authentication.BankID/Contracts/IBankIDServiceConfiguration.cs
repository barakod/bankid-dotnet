﻿using Barakod.Authentication.BankID.Models;
using Barakod.Authentication.BankID.Models.Dtos;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Cryptography.X509Certificates;

namespace Barakod.Authentication.BankID.Contracts
{
    public interface IBankIDServiceConfiguration
    {
        Uri AuthEndpoint { get; }
        Uri SignEndpoint { get; }
        Uri CollectEndpoint { get; }
        Uri CancelEndpoint { get; }
        Requirement Requirement { get; }
    }
}
﻿using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Models.Dtos;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID
{
    /// <summary>
    /// Service for communicating with Swedish Mobilt BankID
    /// </summary>
    public interface IBankIDService
    {
        /// <summary>
        /// Initiates an authentication or signing order. 
        /// Use the collect method to query the status of the order. 
        /// If the request is successful, HTTP 200, the orderRef and autoStartToken is returned. 
        /// Example request auth without personal number.
        /// </summary>
        /// <param name="request">AuthRequest payload with request information</param>
        /// <returns>AuthSignResponse with OrderRef to collect status</returns>
        /// <remarks>
        /// <see cref="AuthRequest"/>
        /// <see cref="AuthSignResponse"/>
        /// </remarks>
        Task<AuthSignResponse> BeginAuthRequest(AuthRequest request);

        /// <summary>
        /// Initiates an authentication or signing order. Use the collect method to query the status of the order. 
        /// If the request is successful, HTTP 200, the orderRef and autoStartToken is returned. 
        /// Example request auth without personal number.
        /// </summary>
        /// <param name="request">SignRequest payload with request information</param>
        /// <returns>AuthSignResponse with OrderRef to collect status and AutoStartToken for autostart BankID app</returns>
        /// <remarks>
        /// <see cref="SignRequest"/>
        /// <see cref="AuthSignResponse"/>
        /// </remarks>
        Task<AuthSignResponse> BeginSignRequest(SignRequest request);

        /// <summary>
        /// Collects the result of a sign or auth order using the orderRef as reference. 
        /// RP should keep on calling collect every two seconds as long as status indicates pending.
        /// RP must abort if status indicates failed. 
        /// The user identity is returned when complete.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <remarks>
        /// <see cref="CollectAuthRequest"/>
        /// <see cref="CollectAuthResponse"/>
        /// </remarks>
        Task<CollectAuthResponse> CollectRequestStatus(CollectAuthRequest request);

        /// <summary>
        /// Cancels an ongoing sign or auth order.This is typically used if the user cancels the order in your service or app.
        /// </summary>
        /// <param name="request">The orderRef from the response from auth or sign. String.</param>
        /// <returns>True if sucessfully canceled</returns>
        /// <remarks>
        /// <see cref="CancelAuthRequest"/>
        /// </remarks>
        Task<bool> CancelRequest(CancelAuthRequest request);
    }
}

﻿using Barakod.Authentication.BankID.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID.Contracts
{
    public interface IBankIDHttpClientService
    {
        Task<T> PostAsync<T>(Uri endpoint, StringContent content);
        Task<bool> Post(Uri endpoint, StringContent content);
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class GenericConverters
    {
        public static string TryObjectToJsonString<T>(this T request)
        {
            try
            {
                return JsonConvert.SerializeObject(request, Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                });

            }
            catch (Exception)
            {
                return request.ToString();
            }
        }
    }
}

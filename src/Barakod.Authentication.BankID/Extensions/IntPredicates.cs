﻿namespace Barakod.Authentication.BankID.Extensions
{
    public static class IntPredicates
    {
        public static bool LessThanOrEqualTo(this int value, int value2)
        {
            return value <= value2;
        }

        public static bool NotZero(this int value)
        {
            return value != 0;
        }
    }
}

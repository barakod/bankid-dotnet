﻿using Barakod.Authentication.BankID.Contracts;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class IBankIDHttpClientServiceConfigurationFactory
    {
        public static HttpClientHandler CreateBankIDClientHandler(IBankIDHttpClientServiceConfiguration configuration)
        {

            var handler = new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                SslProtocols = SslProtocols.Tls12,
                ServerCertificateCustomValidationCallback = (httpRequest, incomingCert, certChain, policy) =>
                    ValidateBankIdRootCertChain(configuration.RootCert, httpRequest, incomingCert, certChain, policy)
            };

            handler.ClientCertificates.AddRange(configuration.PrivateCert);

            return handler;
        }

        private static bool ValidateBankIdRootCertChain(X509Certificate2 validRootCert, HttpRequestMessage httpRequest, X509Certificate2 incomingCert, X509Chain certChain, SslPolicyErrors policy)
        {
            var newChain = new X509Chain
            {
                ChainPolicy =
                {
                    VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority,
                    RevocationMode = X509RevocationMode.NoCheck
                }
            };

            newChain.ChainPolicy.ExtraStore.Add(validRootCert);

            var isChainValid = newChain.Build(incomingCert);
            var incomingChainRoot = newChain.ChainElements[newChain.ChainElements.Count - 1].Certificate;
            var isChainRootCertificateAuthority = incomingChainRoot.RawData.SequenceEqual(validRootCert.RawData);

            return isChainValid && isChainRootCertificateAuthority;
        }

    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class StringBuilders
    {
        public static string FormatErrorMessage(this string memberName, string sourceFilePath, int sourceLineNumber)
        {
            string filename = sourceFilePath.ExtractFileName();
            string caller = memberName.FormatMember();
            var linenumber = sourceLineNumber.ToString().PadLeft(4);
            return $">>>> { filename }:Line { linenumber } >>>> { caller }()";
        }

        public static string ExtractFileName(this string sourceFilePath)
        {
            return sourceFilePath.Split('\\', '/').Last();
        }

        public static string FormatMember(this string memberName)
        {
            return memberName.Trim('.');
        }

        public static string AppendMessage(this string message, string messageToAppend)
        {
            return string.Concat(message, " >>> MESSAGE: ", messageToAppend);
        }

        public static string ThrowArgumentException(this string message)
        {
            throw new ArgumentException(message);
        }

        public static string ToBase64(this string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }

        public static string FormatValidSwedishPersonalTo12Digits(this string personalNumber)
        {
            if (personalNumber == null) return personalNumber;

            string RegExForValidation = @"^(?<date>\d{6}|\d{8})[-\s]?(?<lastFour>\d{4})$";
            var result = Regex.Match(personalNumber, RegExForValidation);
            string lastFour = result.Groups["lastFour"].Value;

            var date = result.Groups["date"].Value.ToDateTime();
            return date.ToString("yyyyMMdd") + lastFour;
        }
    }
}

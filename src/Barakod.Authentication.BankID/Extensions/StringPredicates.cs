﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class StringPredicates
    {
        public static bool NotValidSwedishPersonalNumberFormat(this string value)
        {
            return !ValidSwedishPersonalNumberFormat(value);
        }

        public static bool ValidSwedishPersonalNumberFormat(this string value)
        {
            string RegExForValidation = @"^(?<date>\d{6}|\d{8})[-\s]?(?<lastFour>\d{4})$";
            var result = Regex.Match((string)value, RegExForValidation);
            var date = result.Groups["date"].Value.ToDateTime();

            return result.Success && result.Groups.Count == 3 && date != default(DateTime);
        }

        public static bool IsNotCorrectControlNumberIn12DigitPersonal(this string personalNumber)
        {
            return !Is12DigitPersonalControlNumberCorrect(personalNumber);
        }

        public static bool Is12DigitPersonalControlNumberCorrect(this string personalNumber)
        {
            var firstNine = string.Concat(personalNumber.Skip(2).Take(9));

            var sb = new StringBuilder();
            for (int i = 1; i <= firstNine.Length; i++)
            {
                var currentNumber = int.Parse(firstNine[i - 1].ToString());
                if (i % 2 != 0)
                {
                    sb.Append(currentNumber * 2);
                }
                else
                {
                    sb.Append(currentNumber);
                }
            }

            var calculate = sb.ToString();
            var sum = 0;
            for (int i = 0; i < calculate.Length; i++)
            {
                sum += int.Parse(calculate[i].ToString());
            }

            var correctControllNumber = (10 - (sum % 10)) % 10;
            var controllNumber = int.Parse(personalNumber.Last().ToString());

            return correctControllNumber == controllNumber;
        }

        public static bool IsBase64EncodedUtf8String(this string str)
        {
            try
            {
                if (str == null) return false;
                var bytes = Convert.FromBase64String(str);
                var toString = Encoding.UTF8.GetString(bytes);
                return true;
            }
            catch (ArgumentNullException) { return false; }
            catch (ArgumentException) { return false; }
            catch (FormatException) { return false; }
        }
        public static bool IsAnyOf(this string str, params string[] stringCollection) => stringCollection.Any(x=> x == str);
        public static bool IsNotAnyOf(this string str, params string[] stringCollection) => !IsAnyOf(str, stringCollection);
        public static bool IsNot(this string str, string str2) => !str.Equals(str2);
        public static bool IsDefault(this string str) => str == default(string);
    }
}

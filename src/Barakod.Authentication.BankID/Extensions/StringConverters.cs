﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Text;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class StringConverters
    {
        public static DateTime ToDateTime(this string date)
        {
            if (DateTime.TryParseExact(date, new[] { "yyMMdd", "yyyyMMdd" }, CultureInfo.InvariantCulture, DateTimeStyles.None, out var dt))
            {
                return dt;
            }
            else return default(DateTime);
        }

        public static StringContent JsonStringToStringContent(this string jsonString)
        {
            var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
            stringContent.Headers.ContentType.CharSet = string.Empty;
            return stringContent;
        }
    }
}

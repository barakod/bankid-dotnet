﻿using Microsoft.Extensions.Logging;
using System.Runtime.CompilerServices;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class ILoggerActions
    {
        public static void Enter(this ILogger logger,
                                 [CallerMemberName] string memberName = "",
                                 [CallerFilePath] string sourceFilePath = "",
                                 [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (logger == null)
            {
                return;
            }

            var errorInfo = memberName.FormatErrorMessage(sourceFilePath, sourceLineNumber);
            logger?.LogTrace($"{errorInfo} ENTER");
        }


        public static void Leave(this ILogger logger,
                                 [CallerMemberName] string memberName = "",
                                 [CallerFilePath] string sourceFilePath = "",
                                 [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (logger == null)
            {
                return;
            }

            var initalString = memberName.FormatErrorMessage(sourceFilePath, sourceLineNumber);
            logger?.LogTrace($"{initalString} LEAVE");
        }

        public static T Return<T>(this ILogger logger,
                                  T obj,
                                  [CallerMemberName] string memberName = "",
                                  [CallerFilePath] string sourceFilePath = "",
                                  [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (logger == null)
            {
                return obj;
            }

            var initalString = memberName.FormatErrorMessage(sourceFilePath, sourceLineNumber);
            
            logger.LogInformation($"{initalString} \n\t >>>> RETURNS >>> {obj.TryObjectToJsonString()}\n");

            return obj;
        }

        public static T Error<T>(this ILogger logger,
                                 T info,
                                 [CallerMemberName] string memberName = "",
                                 [CallerFilePath] string sourceFilePath = "",
                                 [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (logger == null) return info;

            var initalString = memberName.FormatErrorMessage(sourceFilePath, sourceLineNumber);
            var formatedMessage = $"{initalString} \n\t ERROR >>> {info.TryObjectToJsonString()}";

            logger.LogError(formatedMessage, info);

            return info;
        }

        public static T LogInfo<T>(this ILogger logger,
                                  T obj,
                                  [CallerMemberName] string memberName = "",
                                  [CallerFilePath] string sourceFilePath = "",
                                  [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (logger == null)
            {
                return obj;
            }

            var initalString = memberName.FormatErrorMessage(sourceFilePath, sourceLineNumber);

            logger.LogInformation($"{initalString} \n\t >>>> INFO >>> {obj.TryObjectToJsonString()}\n");

            return obj;
        }

        public static T LogInfo<T>(this T obj, ILogger logger,
                          [CallerMemberName] string memberName = "",
                          [CallerFilePath] string sourceFilePath = "",
                          [CallerLineNumber] int sourceLineNumber = 0)
        {
            if (logger == null)
            {
                return obj;
            }

            var initalString = memberName.FormatErrorMessage(sourceFilePath, sourceLineNumber);

            logger.LogInformation($"{initalString} \n\t INFO >>> {obj.TryObjectToJsonString()}\n");

            return obj;
        }
    }
}

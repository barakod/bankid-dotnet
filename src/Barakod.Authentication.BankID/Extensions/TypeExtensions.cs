﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class TypeExtensions
    {
        public static byte[] ExtractEmbeddedResourceFromTypeAsByteArray(this Type type, string resourceName)
        {
            var thisAssambly = type.Assembly;
            var name = thisAssambly.GetManifestResourceNames()
                .FirstOrDefault(fullName => fullName.EndsWith(resourceName));

            if (name == null)
            {
                throw new Exception($"No resource found at {thisAssambly} " +
                    $"with name {resourceName}");
            }

            using (var stream = thisAssambly.GetManifestResourceStream(name))
            {

                byte[] byteArray = new byte[stream.Length];
                for (int Index = 0; Index < stream.Length; Index++)
                {
                    byteArray[Index] = (byte)stream.ReadByte();
                }

                return byteArray;
            }
        }
    }
}

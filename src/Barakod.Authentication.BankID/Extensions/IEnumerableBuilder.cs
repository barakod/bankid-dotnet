﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class IEnumerableBuilder
    {
        public static IEnumerable<ArgumentException> ToArgumentExceptions(this IEnumerable<string> errorMessages)
        {
            return errorMessages.Select(msg => new ArgumentException(msg));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class IEnumerableActions
    {
        public static void ThrowAggregateIfAny(this IEnumerable<Exception> errorMessages)
        {
            var test = errorMessages.ToList();
            if(test.Count > 0)
            throw new AggregateException(test);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class CertificatePredicates
    {
        public static bool HasThumbPrint(this X509Certificate2 cert)
        {
            try
            {
                return !string.IsNullOrEmpty(cert?.Thumbprint);
            }
            catch (CryptographicException)
            {
                return false;
            }
        }

        public static bool AllCertsHasThumbPrint(this X509Certificate2Collection certCollection)
        {
            try
            {
                if (certCollection == null) return false;
                var allCertsHasTumbprint = false;
                foreach (var cert in certCollection)
                {
                    allCertsHasTumbprint = cert.HasThumbPrint();
                }
                return allCertsHasTumbprint;
            }
            catch (CryptographicException)
            {
                return false;
            }
        }

        public static bool IsNotDefault(this X509Certificate2 certificate2)
        {
            return certificate2 != default(X509Certificate2);

        }
    }
}

﻿using Barakod.Authentication.BankID.Constants;
using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Extensions;
using Barakod.Authentication.BankID.Models.Dtos;
using Microsoft.Extensions.Logging;
using System;

namespace Barakod.Authentication.BankID
{
    public sealed class BankIDConfiguration : IBankIDServiceConfiguration
    {
        public BankIDConfiguration(Requirement requirement = null, ILogger<BankIDConfiguration> logger = null)
        {
            logger.Enter();

            AuthEndpoint = new Uri(BankIDApiEndPoints.Auth.LogInfo(logger), UriKind.Relative);
            SignEndpoint = new Uri(BankIDApiEndPoints.Sign.LogInfo(logger), UriKind.Relative);
            CollectEndpoint = new Uri(BankIDApiEndPoints.Collect.LogInfo(logger), UriKind.Relative);
            CancelEndpoint = new Uri(BankIDApiEndPoints.Cancel.LogInfo(logger), UriKind.Relative);
            Requirement = requirement.LogInfo(logger);

            logger.Leave();
        }

        public Uri AuthEndpoint { get; }

        public Uri SignEndpoint { get; }

        public Uri CollectEndpoint { get; }

        public Uri CancelEndpoint { get; }

        public Requirement Requirement { get; }
    }
}

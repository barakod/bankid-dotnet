﻿using Barakod.Authentication.BankID.Constants;
using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Exceptions;
using Barakod.Authentication.BankID.Extensions;
using Barakod.Authentication.BankID.Models.Dtos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID.Services
{
    public class BankIDHttpClientService : IBankIDHttpClientService
    {
        private HttpClient _client;
        private ILogger _logger;
        private readonly IBankIDHttpClientServiceConfiguration _configuration;

        private BankIDHttpClientService()
        {

        }
        public BankIDHttpClientService(HttpClient client, ILogger<BankIDHttpClientService> logger = null)
        {
            logger.Enter();

            _logger = logger;
            CheckForErrors(client).ToArgumentExceptions().ThrowAggregateIfAny();
            _client = client;

            _logger.Leave();
        }



        public BankIDHttpClientService(IBankIDHttpClientServiceConfiguration configuration)
        {
            _logger.Enter();

            CheckForErrors(configuration).ToArgumentExceptions().ThrowAggregateIfAny();
            _configuration = configuration;
            var handler = IBankIDHttpClientServiceConfigurationFactory.CreateBankIDClientHandler(configuration);
            _client = new HttpClient(handler) { BaseAddress = configuration.BaseUrl };

            _logger.Leave();
        }


        public async Task<bool> Post(Uri endpoint, StringContent content)
        {
            _logger.Enter();
            var result = await _client.PostAsync(endpoint, content);
            return _logger.Return(result.IsSuccessStatusCode);
        }


        public async Task<T> PostAsync<T>(Uri endpoint, StringContent content)
        {
            _logger.Enter();
            var result = await _client.PostAsync(endpoint, content);

            var contents = await result.Content.ReadAsStringAsync();
            if (result.IsSuccessStatusCode)
            {

                return _logger.Return(JsonConvert.DeserializeObject<T>(contents));
            }
            else
            {
                try
                {
                    var error = JsonConvert.DeserializeObject<Error>(contents);
                    _logger.Leave();
                    throw new BankIDException(error);
                }
                catch (JsonSerializationException e)
                {
                    _logger.Leave();
                    throw new BankIDException(new Error { Details = contents }, e);
                }
            }
        }

        private IEnumerable<string> CheckForErrors(IBankIDHttpClientServiceConfiguration configuration)
        {
            _logger.Enter();

            if (configuration == null)
            {
                yield return _logger.Error($"{nameof(configuration)} can not be null");
            }

            _logger.Leave();
        }

        private IEnumerable<string> CheckForErrors(HttpClient client)
        {
            _logger.Enter();
            if (client == default(HttpClient))
            {
                yield return _logger.Error($"{nameof(client)}: HttpClient can not be null");
            }
            if (client?.BaseAddress?.ToString().IsDefault() ?? true)
            {
                yield return _logger.Error($"{nameof(client.BaseAddress)}: No BaseAddress present.");
            }
            var test = client?.BaseAddress?.ToString();
            if (client?.BaseAddress?.ToString().IsNotAnyOf(BankIDApiBaseUrls.Production, BankIDApiBaseUrls.Test) ?? false)
            {
                yield return _logger.Error($"{nameof(client.BaseAddress)}: Not a valid BaseAddress.");
            }
            _logger.Leave();
        }
    }
}

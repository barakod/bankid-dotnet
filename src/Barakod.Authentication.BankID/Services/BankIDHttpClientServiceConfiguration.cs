﻿using Barakod.Authentication.BankID.Constants;
using Barakod.Authentication.BankID.Extensions;
using Barakod.Authentication.BankID.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Barakod.Authentication.BankID.Contracts
{
    public class BankIDHttpClientServiceConfiguration : IBankIDHttpClientServiceConfiguration
    {
        public BankIDHttpClientServiceConfiguration(BankIDApiEnvironment environment,
                                                    X509Certificate2Collection privateCert = null,
                                                    X509Certificate2 rootCert = null)
        {
            CheckForErrors(environment, privateCert, rootCert).ToArgumentExceptions().ThrowAggregateIfAny();


            Environment = environment;
            PrivateCert = privateCert ?? GetTestBankIDPrivateCert();
            RootCert = rootCert ?? GetTestBankIDRootCert();

            var baseUrl = environment == BankIDApiEnvironment.ProductionV322
                ? BankIDApiBaseUrls.Production
                : BankIDApiBaseUrls.Test;
            BaseUrl = new Uri(baseUrl, UriKind.Absolute);

        }
        public Uri BaseUrl { get; }

        public X509Certificate2 RootCert { get; }

        public X509Certificate2Collection PrivateCert { get; }

        public BankIDApiEnvironment Environment { get; }

        public static X509Certificate2 GetTestBankIDRootCert()
        {
            var bytes = typeof(BankIDConfiguration)
                .ExtractEmbeddedResourceFromTypeAsByteArray("TestRootCert.crt");

            return new X509Certificate2(bytes);
        }

        public static X509Certificate2Collection GetTestBankIDPrivateCert()
        {
            var bytes = typeof(BankIDConfiguration)
                .ExtractEmbeddedResourceFromTypeAsByteArray("TestCertificate.pfx");

            var collection = new X509Certificate2Collection();
            collection.Import(bytes, "qwerty123", X509KeyStorageFlags.PersistKeySet);
            return collection;
        }



        private IEnumerable<string> CheckForErrors(BankIDApiEnvironment environment,
                                            X509Certificate2Collection privateCert = null,
                                            X509Certificate2 rootCert = null)
        {
            if (!Enum.IsDefined(environment.GetType(), environment))
            {
                yield return $"{nameof(environment)} has not a valid option";
            }

            if (environment == BankIDApiEnvironment.ProductionV322 && (privateCert == null || rootCert == null))
            {
                yield return $"Production environment must have custom certificates";
            }

            if (environment == BankIDApiEnvironment.TestV322 && (privateCert != null || rootCert != null))
            {
                yield return $"No custom certificates is allowed for Test environment";
            }

            if (privateCert?.Count.NotZero() ?? false)
            {
                yield return $"{nameof(privateCert)} has no certificates";
            }

            if (privateCert?.AllCertsHasThumbPrint() ?? false)
            {
                yield return $"There is certs without thumbprint in {nameof(privateCert)}";
            }

            if (!rootCert?.HasThumbPrint() ?? false)
            {
                yield return $"No thumbprint found in {nameof(rootCert)}";
            }
        }
    }
}
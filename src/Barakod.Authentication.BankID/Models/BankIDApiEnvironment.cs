﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Barakod.Authentication.BankID.Models
{
    /// <summary>
    /// https://www.bankid.com/assets/bankid/rp/bankid-relying-party-guidelines-v3.2.2.pdf
    /// <para>Api version /rp/v5/</para>
    /// </summary>
    public enum BankIDApiEnvironment
    {
        TestV322 = 1,
        ProductionV322 = 2
    }
}

﻿using System.Runtime.Serialization;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    public enum CertificatePolicie
    {
        [EnumMember(Value = null)]
        Default = 0,
        [EnumMember(Value = "1.2.752.78.1.1")]
        ProdBankIDOnFile,
        [EnumMember(Value = "1.2.752.78.1.2")]
        ProdBankIDOnSmartCard,
        [EnumMember(Value = "1.2.752.78.1.5")]
        ProdMobileBankID,
        [EnumMember(Value = "1.2.752.71.1.3")]
        NordeaEIdOnFileAndOnSmartCard,
        [EnumMember(Value = "1.2.3.4.5")]
        TestBankIDOnFile,
        [EnumMember(Value = "1.2.3.4.10")]
        TestBankIDOnSmartCard,
        [EnumMember(Value = "1.2.3.4.25")]
        TestMobileBankID,
        [EnumMember(Value = "1.2.752.60.1.6")]
        TestBankIDForSomeBankIDBanks,
    }
}

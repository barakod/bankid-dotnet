﻿using System.Runtime.Serialization;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    /// <summary>
    /// Only present for pending and failed orders.
    /// default: unknown hint code, more hint codes can be added over time
    /// </summary>
    public enum HintCode
    {
        [EnumMember(Value = "default")]
        Default = 0,
        [EnumMember(Value = "outstandingTransaction")]
        PendingOutstandingTransaction,
        [EnumMember(Value = "noClient")]
        PendingNoClient,
        [EnumMember(Value = "started")]
        PendingStarted,
        [EnumMember(Value = "userSign")]
        PendingUserSign,
        [EnumMember(Value = "expiredTransaction")]
        FailedExpiredTransaction,
        [EnumMember(Value = "certificateErr")]
        FailedCertificateErr,
        [EnumMember(Value = "userCancel")]
        FailedUserCancel,
        [EnumMember(Value = "cancelled")]
        FailedCancelled,
        [EnumMember(Value = "startFailed")]
        FailedStart,
    }
}

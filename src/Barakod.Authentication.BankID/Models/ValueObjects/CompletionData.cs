﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    /// <summary>
    /// This is a final state. The order was successful. The user has provided the security code and completed the
    /// order.The completionData includes the signature, user information and the OCSP response.RP should
    /// control the user information and continue their process. RP should keep the completion data for future
    /// references/compliance/audit
    /// </summary>
    public class CompletionData
    {
        /// <summary>
        /// Information related to the user.
        /// </summary>
        [JsonRequired]
        [JsonProperty("user")]
        public User User { get; set; }

        /// <summary>
        /// Information related to the device
        /// </summary>
        [JsonRequired]
        [JsonProperty("device")]
        public Device Device { get; set; }

        /// <summary>
        /// Information related to the device
        /// </summary>
        [JsonRequired]
        [JsonProperty("cert")]
        public Cert Cert { get; set; }

        /// <summary>
        /// The signature. The content of the signature is described in BankID Signature Profile specification.
        /// String.Base64-encoded.XML signature.
        /// </summary>
        [JsonRequired]
        [JsonProperty("signature")]
        public string Signature { get; set; }

        /// <summary>
        /// The OCSP response.String.Base64-encoded.The OCSP response is signed by a certificate that
        /// has the same issuer as the certificate being verified. The OSCP response has an extension for
        /// Nonce.The nonce is calculated as:
        ///  SHA-1 hash over the base 64 XML signature encoded as UTF-8.
        ///  12 random bytes is added after the hash
        ///  The nonce is 32 bytes (20 + 12)
        /// </summary>
        [JsonRequired]
        [JsonProperty("ocspResponse")]
        public string OcspResponse { get; set; }

    }
}

﻿using System.Runtime.Serialization;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    public enum IssuerCn
    {
        [EnumMember(Value = null)]
        Default = 0,
        [EnumMember(Value = "Nordea CA for Smartcard users 12")]
        NordeaCASmartCard12,
        [EnumMember(Value = "Nordea CA for Softcert users 13")]
        NordeaCASoftCert13,
        [EnumMember(Value = "Nordea Test CA for Smartcard users 12")]
        NordeaTestCASmartCard12,
        [EnumMember(Value = "Nordea Test CA for Softcert users 13")]
        NordeaTestCASoftCert13
    }
}

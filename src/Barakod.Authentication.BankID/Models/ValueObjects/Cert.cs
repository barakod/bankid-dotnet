﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    /// <summary>
    /// Information related to the users certificate (BankID), holds the following children:
    ///  notBefore: Start of validity of the users BankID.String, Unix ms.
    ///  notAfter: End of validity of the Users BankID.String, Unix ms.
    /// Note: notBefore and notAfter are the number of milliseconds since the UNIX Epoch, a.k.a.
    /// "UNIX time" in milliseconds.It was chosen over ISO8601 for its simplicity and
    /// </summary>
    public class Cert
    {
        /// <summary>
        /// notBefore: Start of validity of the users BankID. String, Unix ms.
        /// </summary>
        [JsonRequired]
        [JsonProperty("notBefore")]
        public string NotBefore { get; set; }

        /// <summary>
        /// notBefore: Start of validity of the users BankID. String, Unix ms.
        /// </summary>
        [JsonRequired]
        [JsonProperty("notAfter")]
        public string NotAfter { get; set; }
    }
}

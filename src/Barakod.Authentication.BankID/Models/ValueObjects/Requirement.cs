﻿using Barakod.Authentication.BankID.Models.ValueObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class Requirement
    {
        [JsonProperty("cardReader")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CardReader? CardReader { get; set; }

        [JsonProperty("certificatePolicies", ItemConverterType = typeof(StringEnumConverter))]
        //[JsonConverter(typeof(StringEnumConverter))]

        public CertificatePolicie[] CertificatePolicies { get; set; }

        [JsonProperty("issuerCn", ItemConverterType = typeof(StringEnumConverter))]
        //[JsonConverter(typeof(StringEnumConverter))]
        public IssuerCn[] IssuerCn { get; set; }

        [JsonProperty("autoStartTokenRequired")]
        public bool? AutoStartTokenRequired { get; set; }

        [JsonProperty("allowFingerprint")]
        public bool? AllowFingerprint { get; set; }

    }
}

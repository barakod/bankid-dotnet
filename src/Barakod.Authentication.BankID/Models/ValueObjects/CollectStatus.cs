﻿using System.Runtime.Serialization;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    /// <summary>
    /// default: Something went wrong.
    /// pending: The order is being processed. hintCode describes the status of the order.
    /// failed: Something went wrong with the order.hintCode describes the error.
    /// complete: The order is complete.completionData holds user information
    /// </summary>
    public enum CollectStatus
    {
        [EnumMember(Value = "default")]
        Default = 0,
        [EnumMember(Value = "pending")]
        Pending,
        [EnumMember(Value = "failed")]
        Failed,
        [EnumMember(Value = "complete")]
        Complete
    }
}

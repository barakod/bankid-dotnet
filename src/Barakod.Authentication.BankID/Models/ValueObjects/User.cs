﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    /// <summary>
    /// Information related to the user
    /// </summary>
    public class User
    {
        /// <summary>
        /// The personal number.
        /// </summary>
        [JsonRequired]
        [JsonProperty("personalNumber")]
        public string PersonalNumber { get; set; }

        /// <summary>
        /// The given name and surname of the user.
        /// </summary>
        [JsonRequired]
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The given name of the user
        /// </summary>
        [JsonRequired]
        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        /// <summary>
        /// The surname of the user
        /// </summary>
        [JsonRequired]
        [JsonProperty("surname")]
        public string SurName { get; set; }
    }
}

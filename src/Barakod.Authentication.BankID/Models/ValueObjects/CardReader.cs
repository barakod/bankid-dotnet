﻿namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    public enum ErrorCode
    {
        AlreadyInProgress,
        InvalidParameters,
        Unauthorized,
        NotFound,
        RequestTimeout,
        unsupportedMediaType,
        InternalError,
        Maintenance,

    }
}

﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.ValueObjects
{
    public class Device
    {
        /// <summary>
        /// The device ip-adress.
        /// </summary>
        [JsonRequired]
        [JsonProperty("ipAddress")]
        public string IpAddress { get; set; }
    }
}

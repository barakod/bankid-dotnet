﻿using Barakod.Authentication.BankID.Models.ValueObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class Error
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ErrorCode ErrorCode { get; set; }

        public string Details { get; set; }

    }
}

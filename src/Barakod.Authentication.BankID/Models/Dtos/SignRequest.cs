﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class SignRequest : AuthRequest
    {
        /// <summary>
        /// The text to be displayed and signed. String. The text can be formatted using CR,
        /// LF and CRLF for new lines.The text must be encoded as UTF-8 and then base 64
        /// encoded. 1--40 000 characters after base 64 encoding
        /// </summary>
        [JsonRequired]
        [JsonProperty("userVisibleData")]
        public string UserVisibleData { get; set; }

        /// <summary>
        /// Data not displayed to the user. String. The value must be base 64-encoded. 
        /// 1-200 000 characters after base 64-encoding
        /// </summary>
        [JsonProperty("userNonVisibleData")]
        public string UserNonVisibleData { get; set; }
    }
}

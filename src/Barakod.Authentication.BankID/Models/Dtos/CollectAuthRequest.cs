﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class CollectAuthRequest
    {
        /// <summary>
        /// The orderRef returned from auth or sign
        /// </summary>
        [JsonRequired]
        [JsonProperty("orderRef")]
        public string OrderRef { get; set; }
    }
}

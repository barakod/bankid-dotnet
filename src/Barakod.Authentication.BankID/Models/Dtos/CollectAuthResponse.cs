﻿using Barakod.Authentication.BankID.Models.ValueObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class CollectAuthResponse
    {
        /// <summary>
        /// The orderRef in question.
        /// </summary>
        [JsonRequired]
        [JsonProperty("orderRef")]
        public string OrderRef { get; set; }

        /// <summary>
        /// pending: The order is being processed. hintCode describes the status of the order.
        /// failed: Something went wrong with the order.hintCode describes the error.
        /// complete: The order is complete.completionData holds user information.
        /// </summary>
        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CollectStatus Status { get; set; }

        /// <summary>
        /// Only present for pending and failed orders. See below.
        /// </summary>
        [JsonProperty("hintCode")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HintCode HintCode { get; set; }



        /// <summary>
        /// Only present for complete orders. See below
        /// </summary>
        [JsonProperty("completionData")]
        public CompletionData CompletionData { get; set; }
    }
}

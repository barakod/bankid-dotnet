﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class AuthSignResponse
    {
        /// <summary>
        /// Used as reference to this order when the client is started automatically. String
        /// </summary>
        [JsonProperty("autoStartToken")]
        public string AutoStartToken { get; set; }

        /// <summary>
        /// Used to collect the status of the order. String.
        /// </summary>
        [JsonRequired]
        [JsonProperty("orderRef")]
        public string OrderRef { get; set; }
    }
}

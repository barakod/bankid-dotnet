﻿using Newtonsoft.Json;

namespace Barakod.Authentication.BankID.Models.Dtos
{
    public class AuthRequest
    {
        /// <summary>
        /// The personal number of the user. String. 12 digits. Century must be included.
        /// If the personal number is excluded, the client must be started with the
        /// autoStartToken returned in the response
        /// </summary>
        [JsonProperty("personalNumber")]
        public string PersonalNumber { get; set; }

        /// <summary>
        /// The user IP address as seen by RP. String. IPv4 and IPv6 is allowed.
        /// Note the importance of using the correct IP address.It must be the IP address
        /// representing the user agent(the end user device) as seen by the RP.If there is a
        /// proxy for inbound traffic, special considerations may need to be taken to get the
        /// correct address. In some use cases the IP address is not available, for instance for
        /// voice based services.
        /// </summary>
        [JsonRequired]
        [JsonProperty("endUserIp")]
        public string EndUserIp { get; set; }

        /// <summary>
        /// Requirements on how the auth or sign order must be performed. See below
        /// RP may use the requirement parameter to describe how the signature must be created and verified. A
        /// typical use case is to require Mobile BankID or a special card reader. 
        /// A requirement can be set for both auth and sign orders.The following table describes requirements, 
        /// their possible values and defaults.
        /// </summary>
        [JsonProperty("requirement")]
        public Requirement Requirement { get; set; }

        //public IEnumerable<string> Validate()
        //{
        //    return ValidationBuilder
        //      .ValidateWithRules(argument: EndUserIp, name: nameof(EndUserIp), StringRules.EndUserIpIsNotNull)
        //      .Add(argument: PersonalNumber, name: nameof(PersonalNumber), StringRules.ValidPersonalNumberFormat, StringRules.ValidControlNumber);
        //}
    }
}

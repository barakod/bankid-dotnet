﻿using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Extensions;
using Barakod.Authentication.BankID.Models.Dtos;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID
{
    /// <summary>
    /// Api class wrapping Swedish Mobilt BankID
    /// </summary>
    public sealed class BankIDService : IBankIDService
    {
        private readonly ILogger _logger;
        private readonly IBankIDHttpClientService _client;
        private readonly IBankIDServiceConfiguration _configuration;

        /// <summary>
        /// Implements <see cref="IBankIDService"/>
        /// </summary>
        /// <param name="client">Service for communicating with Http requests</param>
        /// <param name="configuration">Settings for endpoint and requirements</param>
        /// <param name="logger">Optional logger for logging information and errors</param>
        public BankIDService(IBankIDHttpClientService client,
                             IBankIDServiceConfiguration configuration,
                             ILogger<BankIDService> logger = null)
        {
            logger.Enter();

            var configurationErrorMessages = CheckForErrors(configuration);
            var clientErrorMessages = CheckForErrors(client);

            configurationErrorMessages
                .Concat(clientErrorMessages)
                .ToArgumentExceptions()
                .ThrowAggregateIfAny();

            _logger = logger;
            _client = client;
            _configuration = configuration;

            _logger.Leave();
        }

        /// <inheritdoc/>
        public async Task<AuthSignResponse> BeginAuthRequest(AuthRequest request)
        {
            _logger.Enter();

            CheckForErrors(request)
                .ToArgumentExceptions()
                .ThrowAggregateIfAny();

            var payload = new AuthRequest
            {
                EndUserIp = request.EndUserIp,
                PersonalNumber = request.PersonalNumber.FormatValidSwedishPersonalTo12Digits(),
                Requirement = request.Requirement ?? _configuration.Requirement
            };

            var content = payload
                .TryObjectToJsonString()
                .JsonStringToStringContent()
                .LogInfo(_logger);

            return _logger.Return(await _client.PostAsync<AuthSignResponse>(_configuration.AuthEndpoint, content));
        }

        /// <inheritdoc/>
        public async Task<AuthSignResponse> BeginSignRequest(SignRequest request)
        {
            _logger.Enter();

            CheckForErrors(request)
                .ToArgumentExceptions()
                .ThrowAggregateIfAny();

            var payload = new SignRequest
            {
                EndUserIp = request.EndUserIp,
                PersonalNumber = request.PersonalNumber?.FormatValidSwedishPersonalTo12Digits(),
                Requirement = request.Requirement ?? _configuration.Requirement,
                UserNonVisibleData = request.UserNonVisibleData?.ToBase64(),
                UserVisibleData = request.UserVisibleData?.ToBase64()
            };

            var content = payload
                .TryObjectToJsonString()
                .LogInfo(_logger)
                .JsonStringToStringContent();

            return _logger.Return(await _client.PostAsync<AuthSignResponse>(_configuration.SignEndpoint, content));
        }

        /// <inheritdoc/>
        public async Task<CollectAuthResponse> CollectRequestStatus(CollectAuthRequest request)
        {
            _logger.Enter();

            CheckForErrors(request)
                .ToArgumentExceptions()
                .ThrowAggregateIfAny();

            var content = request
                .TryObjectToJsonString()
                .LogInfo(_logger)
                .JsonStringToStringContent();
            var result = await _client.PostAsync<CollectAuthResponse>(_configuration.CollectEndpoint, content);

            return _logger.Return(result);
        }

        /// <inheritdoc/>
        public async Task<bool> CancelRequest(CancelAuthRequest request)
        {
            _logger.Enter();

            CheckForErrors(request)
                .ToArgumentExceptions()
                .ThrowAggregateIfAny();

            var content = request
                .TryObjectToJsonString()
                .LogInfo(_logger)
                .JsonStringToStringContent();

            var result = await _client.Post(_configuration.CancelEndpoint, content);

            return _logger.Return(result);
        }

        private IEnumerable<string> CheckForErrors(AuthRequest request)
        {
            _logger.Enter();

            if (request.EndUserIp == null)
            {
                yield return _logger.Error($"{nameof(request.EndUserIp)} can not be null");
            }

            if (request.PersonalNumber?.ValidSwedishPersonalNumberFormat() ?? false)
            {
                var isValid = request.PersonalNumber
                .FormatValidSwedishPersonalTo12Digits()
                .Is12DigitPersonalControlNumberCorrect();

                if (!isValid)
                {
                    yield return _logger.Error($"{nameof(request.PersonalNumber)} control number is invalid.");
                }
            }

            if (request.PersonalNumber?.NotValidSwedishPersonalNumberFormat() ?? false)
            {
                yield return _logger.Error($"{nameof(request.PersonalNumber)} is not in a correct format.");
            }

            _logger.Leave();
        }

        private IEnumerable<string> CheckForErrors(SignRequest request)
        {
            _logger.Enter();

            foreach (var message in CheckForErrors((AuthRequest)request))
            {
                yield return _logger.Error(message);
            }

            if (request.UserVisibleData == null)
            {
                yield return _logger.Error($"{nameof(request.UserVisibleData)} can not be null");
            }
            else if (!request?.UserVisibleData?.Length.LessThanOrEqualTo(40000) ?? false)
            {
                yield return _logger.Error($"{nameof(request.UserVisibleData)} can not be longer than 40,000 chars");
            }
            else if (!request?.UserVisibleData?.IsBase64EncodedUtf8String() ?? false)
            {
                yield return _logger.Error($"{nameof(request.UserVisibleData)} must be a Base64 encoded UTF8 string");
            }

            if (!request?.UserNonVisibleData?.Length.LessThanOrEqualTo(40000) ?? false)
            {
                yield return _logger.Error($"{nameof(request.UserNonVisibleData)} can not be longer than 40,000 chars");
            }
            else if (!request?.UserNonVisibleData?.IsBase64EncodedUtf8String() ?? false)
            {
                yield return _logger.Error($"{nameof(request.UserNonVisibleData)} must be a Base64 encoded UTF8 string");
            }

            _logger.Leave();
        }

        private IEnumerable<string> CheckForErrors(CollectAuthRequest request)
        {
            _logger.Enter();

            if (request.OrderRef == null)
            {
                yield return _logger.Error($"{nameof(request.OrderRef)} can not be null");
            }

            _logger.Leave();
        }

        private IEnumerable<string> CheckForErrors(CancelAuthRequest request)
        {
            _logger.Enter();

            if (request.OrderRef == null)
            {
                yield return _logger.Error($"{nameof(request.OrderRef)} can not be null");
            }

            _logger.Leave();
        }

        private IEnumerable<string> CheckForErrors(IBankIDHttpClientService client)
        {
            _logger.Enter();

            if (client == null)
            {
                yield return _logger.Error($"{nameof(client)} can not be null");
            }

            _logger.Leave();
        }

        private IEnumerable<string> CheckForErrors(IBankIDServiceConfiguration configuration)
        {
            _logger.Enter();

            if (configuration == null)
            {
                yield return _logger.Error($"{nameof(configuration)} can not be null");
            }

            if (configuration?.AuthEndpoint?.ToString().IsDefault() ?? true)
            {
                yield return _logger.Error($"{nameof(configuration.AuthEndpoint)} can not be null");
            }

            if (configuration?.CollectEndpoint?.ToString().IsDefault() ?? true)
            {
                yield return _logger.Error($"{nameof(configuration.CollectEndpoint)} can not be null");
            }

            if (configuration?.CancelEndpoint?.ToString().IsDefault() ?? true)
            {
                yield return _logger.Error($"{nameof(configuration.CancelEndpoint)} can not be null");
            }

            if (configuration?.SignEndpoint?.ToString().IsDefault() ?? true)
            {
                yield return _logger.Error($"{nameof(configuration.SignEndpoint)} can not be null");
            }

            _logger.Leave();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Barakod.Authentication.BankID.Constants
{
    public static class BankIDApiEndPoints
    {
        public const string Auth = "auth";
        public const string Sign = "sign";
        public const string Collect = "collect";
        public const string Cancel = "cancel";
    }
}

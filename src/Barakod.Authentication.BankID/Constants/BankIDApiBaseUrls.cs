﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Barakod.Authentication.BankID.Constants
{
    public static class BankIDApiBaseUrls
    {
        public const string Production = "https://appapi2.bankid.com/rp/v5/";
        public const string Test = "https://appapi2.test.bankid.com/rp/v5/";
    }
}

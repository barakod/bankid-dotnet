﻿using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Models;
using Barakod.Authentication.BankID.Models.Dtos;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class BankIDConfigurationBuilder
    {
        public static IBankIDServiceConfiguration Build(Requirement requirement = null)
        {
            return new BankIDConfiguration(requirement);
        }

        public static IBankIDHttpClientServiceConfiguration Build(BankIDApiEnvironment environment)
        {
            return new BankIDHttpClientServiceConfiguration(environment);
        }

        public static IBankIDHttpClientServiceConfiguration Build(BankIDApiEnvironment environment, X509Certificate2Collection privateCertificateCollection, X509Certificate2 rootCertificate)
        {
            return new BankIDHttpClientServiceConfiguration(environment, privateCertificateCollection, rootCertificate);
        }

        public static IBankIDHttpClientServiceConfiguration Build(BankIDApiEnvironment environment, string rootCertRelativePath, string privateCertRelativePath, string privateCertPassword)
        {
            (X509Certificate2Collection privateCertificateCollection, X509Certificate2 rootCertificate) =
                InitializeCertificates(rootCertRelativePath, privateCertRelativePath, privateCertPassword);

            return new BankIDHttpClientServiceConfiguration(environment, privateCertificateCollection, rootCertificate);
        }

        public static (X509Certificate2Collection privateCertificate, X509Certificate2 rootCertificate) InitializeCertificates(string rootCertPath, string privateCertPath, string privateCertPassword)
        {
            var absolutePfxCertPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, privateCertPath);
            var privateCertificateCollection = new X509Certificate2Collection();
            privateCertificateCollection.Import(absolutePfxCertPath, privateCertPassword, X509KeyStorageFlags.PersistKeySet);


            var absoluteRootCertPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, rootCertPath);
            var rootCertificate = new X509Certificate2(absoluteRootCertPath);

            return (privateCertificateCollection, rootCertificate);
        }
    }
}

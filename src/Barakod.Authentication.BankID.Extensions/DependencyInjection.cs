﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Text;
using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Services;
using Barakod.Authentication.BankID.Models;
using System.Security.Cryptography.X509Certificates;

namespace Barakod.Authentication.BankID.Extensions
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Adds Transient implementation of <see cref="IBankIDService"/>. 
        /// Require the <see cref="IBankIDService"/> in a class constructor to get access to 
        /// the BankID Api. Use this overload to provide custom implementations of <see cref="IBankIDServiceConfiguration"/>
        /// and <see cref="IBankIDServiceConfiguration"/>. Not recommended.
        /// </summary>
        ///<example>After class initialisation, call this method:
        ///<code>
        ///var account = new Account(10, 2000);
        ///account.Credit(5000);
        ///</code>
        ///</example>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        /// <param name="configuration">Custom configuration of <see cref="IBankIDServiceConfiguration"/></param>
        /// <param name="clientConfiguration">Custom configuration of <see cref="IBankIDHttpClientService"/></param>
        /// <returns><see cref="IServiceCollection"/></returns>
        public static IServiceCollection AddBankId(this IServiceCollection services, IBankIDServiceConfiguration configuration, IBankIDHttpClientServiceConfiguration clientConfiguration)
        {

            services.AddSingleton(configuration);
            services.AddTransient<IBankIDHttpClientService, BankIDHttpClientService>();
            services.AddTransient<IBankIDService, BankIDService>();
            services.AddHttpClient<IBankIDHttpClientService, BankIDHttpClientService>()
                .ConfigurePrimaryHttpMessageHandler(() => IBankIDHttpClientServiceConfigurationFactory.CreateBankIDClientHandler(clientConfiguration))
                .ConfigureHttpClient(c => c.BaseAddress = clientConfiguration.BaseUrl);



            //services.AddSingleton(clientConfiguration);
            return services;
        }

        /// <summary>
        /// Adds Transient implementation of <see cref="IBankIDService"/>. 
        /// Require the <see cref="IBankIDService"/> in a class constructor to get access to 
        /// the BankID Api. Only works for BankID Test environment. Please provide custom certificates for production environment.
        /// </summary>
        /// <code>
        /// // Example:
        /// services.AddBankId(BankIDApiEnvironment.TestV322);
        /// </code>
        /// <param name="services"></param>
        /// <param name="environment">Custom configuration of <see cref="IBankIDService"/></param>
        /// <returns><see cref="IServiceCollection"/></returns>
        public static IServiceCollection AddBankID(this IServiceCollection services,
                                                   BankIDApiEnvironment environment = BankIDApiEnvironment.TestV322)
        {
            if (environment != BankIDApiEnvironment.TestV322) throw new ArgumentException("environment can only be test without custom certificates");
            return AddBankId(services, BankIDConfigurationBuilder.Build(), BankIDConfigurationBuilder.Build(BankIDApiEnvironment.TestV322));
        }

        /// <summary>
        /// Adds Transient implementation of <see cref="IBankIDService"/>. 
        /// Require the <see cref="IBankIDService"/> in a class constructor to get access to 
        /// the BankID Api. Add private pfx cert as a "<see cref="X509Certificate2Collection"/> privateCertificateCollection"
        /// and a "<see cref="X509Certificate2"/> rootCertificate" for the root certificate .crt file.
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        /// <param name="privateCertificateCollection">Custom configuration of <see cref="IBankIDService"/></param>
        /// <param name="rootCertificate">Custom configuration of <see cref="IBankIDHttpClientService"/></param>
        /// <returns><see cref="IServiceCollection"/></returns>
        public static IServiceCollection AddBankId(this IServiceCollection services,
                                                   X509Certificate2Collection privateCertificateCollection,
                                                   X509Certificate2 rootCertificate)
        {
            return AddBankId(services, BankIDConfigurationBuilder.Build(), BankIDConfigurationBuilder.Build(BankIDApiEnvironment.ProductionV322, privateCertificateCollection, rootCertificate));
        }

        /// <summary>
        /// Adds Transient implementation of <see cref="IBankIDService"/>. 
        /// Require the <see cref="IBankIDService"/> in a class constructor to get access to 
        /// the BankID Api. Use this overload to provide certificates located at a 
        /// relative path to the output folder. If any of the <see cref="System.String"/> params is null, 
        /// the service vill fallback to <see cref="BankIDApiEnvironment.TestV322"/>.
        /// </summary>
        /// <code>
        /// // Example:
        /// var rootCertificateRelativePath = Configuration["BANKID_ROOT_CERT_RELATIVE_PATH"];
        /// var privateCertificateRelativePath = Configuration["BANKID_PRIVATE_CERT_RELATIVE_PATH"];
        /// var privateCertificatePassword = Configuration["BANKID_PRIVATE_CERT_PASSWORD"];
        /// var isProductionString = Configuration["BANKID_ENVIRONMENT_IS_PROD"];
        /// 
        /// var environment = isProductionString == null || isProductionString.ToLower() != "production"
        ///    : BankIDApiEnvironment.TestV322;
        ///    ? BankIDApiEnvironment.ProductionV322
        ///
        /// services.AddBankId(environment,
        ///                   rootCertificateRelativePath,
        ///                   privateCertificateRelativePath,
        ///                   privateCertificatePassword);
        /// </code>
        /// <param name="services"><see cref="IServiceCollection"/></param>
        /// <param name="rootCertRelativePath">Custom configuration of <see cref="IBankIDService"/></param>
        /// <param name="privateCertRelativePath">Custom configuration of <see cref="IBankIDHttpClientService"/></param>
        /// <param name="privateCertPassword">Custom configuration of <see cref="IBankIDHttpClientService"/></param>
        /// <returns><see cref="IServiceCollection"/></returns>
        public static IServiceCollection AddBankId(this IServiceCollection services,
                                                   BankIDApiEnvironment environment,
                                                   string rootCertRelativePath,
                                                   string privateCertRelativePath,
                                                   string privateCertPassword)
        {
            if (rootCertRelativePath == null || privateCertPassword == null || privateCertPassword == null)
            {
                return AddBankID(services,environment);
            }
            else
            {
                return AddBankId(services,
                                 BankIDConfigurationBuilder.Build(),
                                 BankIDConfigurationBuilder.Build(BankIDApiEnvironment.ProductionV322, rootCertRelativePath, privateCertRelativePath, privateCertPassword));
            }
        }
    }
}

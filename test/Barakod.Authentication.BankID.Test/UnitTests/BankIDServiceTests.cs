﻿using Barakod.Authentication.BankID.Constants;
using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Exceptions;
using Barakod.Authentication.BankID.Models;
using Barakod.Authentication.BankID.Models.Dtos;
using Barakod.Authentication.BankID.Models.ValueObjects;
using Barakod.Authentication.BankID.Test.Constants;
using Barakod.Authentication.BankID.Test.Fakes;
using FakeItEasy;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Barakod.Authentication.BankID.Test
{
    class BankIDServiceTests
    {
        private IBankIDService _sut;
        private IBankIDServiceConfiguration _config;
        private IBankIDHttpClientService _httpClient;
        private ILogger<BankIDService> _logger;

        [SetUp]
        public void BaseSetup()
        {
            _logger = A.Fake<ILogger<BankIDService>>();
            _httpClient = A.Fake<IBankIDHttpClientService>();
            _config = A.Fake<IBankIDServiceConfiguration>();
        }

        [Test]
        public void Constructor_RunsWithoutException_GivenValidConfig()
        {
            // Act & Assert
            Assert.DoesNotThrow(() => new BankIDService(_httpClient, _config, _logger));
        }

        [Test]
        public void Constructor_TrowsAggregateException_GivenConfigWithoutInitializedAuthEndpoint()
        {
            // Arrange
            A.CallTo(() => _config.AuthEndpoint).Returns(default);

            // Act & Assert
            Assert.Throws<AggregateException>(() => new BankIDService(_httpClient, _config, _logger));

        }

        [Test]
        public void Constructor_TrowsAggregateException_GivenConfigWithoutInitializedCancelEndpoint()
        {
            // Arrange
            A.CallTo(() => _config.CancelEndpoint).Returns(default);

            // Act & Assert
            Assert.Throws<AggregateException>(() => new BankIDService(_httpClient, _config, _logger));
        }

        [Test]
        public void Constructor_TrowsAggregateException_GivenConfigWithoutInitializedCollectEndpoint()
        {
            // Arrange
            A.CallTo(() => _config.CollectEndpoint).Returns(default);

            // Act & Assert
            Assert.Throws<AggregateException>(() => new BankIDService(_httpClient, _config, _logger));
        }

        [Test]
        public void Constructor_TrowsAggregateException_GivenConfigWithoutInitializedSignEndpoint()
        {
            // Arrange
            A.CallTo(() => _config.SignEndpoint).Returns(default);

            // Act & Assert
            Assert.Throws<AggregateException>(() => new BankIDService(_httpClient, _config, _logger));
        }

        [TestCase("19900115_3874")]
        [TestCase("19900115 3874 ")]
        [TestCase("1990011-53874")]
        [TestCase(" 9001153874")]
        [TestCase("1900115 3874")]
        [TestCase("900115-3873")]
        [TestCase("9001153872")]
        [TestCase("050115-9379")]
        [TestCase("0501259370")]
        [TestCase("0513159370")]
        [TestCase("200501329370")]
        [TestCase("20050115-9375")]
        [TestCase("20050116-9370")]
        public void BeginAuthRequest_ThrowsAggregateException_GivenInvalidPersonal(string personal)
        {
            // Arrange
            var payload = new AuthRequest
            {
                EndUserIp = TestValidValues.EndUserIp,
                PersonalNumber = personal
            };

            // Act & Assert
            Assert.ThrowsAsync<AggregateException>(async ()=> {
                await _sut.BeginAuthRequest(payload); });
        }

        [TestCase(null)]
        [TestCase("19900115-3874")]
        [TestCase("19900115 3874")]
        [TestCase("199001153874")]
        [TestCase("9001153874")]
        [TestCase("900115 3874")]
        [TestCase("900115-3874")]
        [TestCase("050115-9370")]
        [TestCase("050115 9370")]
        [TestCase("0501159370")]
        [TestCase("200501159370")]
        [TestCase("20050115-9370")]
        [TestCase("20050115-9370")]
        public void BeginAuthRequest_DoesNotThrow_GivenValidPersonal(string personal)
        {
            // Arrange

            _sut = new BankIDService(_httpClient, _config, _logger);

            var payload = new AuthRequest
            {
                EndUserIp = TestValidValues.EndUserIp,
                PersonalNumber = personal
            };

            // Act & Assert
            Assert.DoesNotThrowAsync(async () => {
                await _sut.BeginAuthRequest(payload);
            });
        }

        [Test]
        public void BeginAuthRequest_TrowsBankIDException_GivenRequestForPersonalNumberAlreadyInProgress()
        {
            // Arrange
            var httpClient = FakeHttpClientFactory
                .Build("Fake400FailedAuthResponsePnAlreadyInProgress", HttpStatusCode.BadRequest);

            _sut = new BankIDService(httpClient, _config, _logger);

            var payload = new AuthRequest
            {
                EndUserIp = TestValidValues.EndUserIp,
                PersonalNumber = TestValidValues.PersonalNumber
            };

            // Act & Assert
            Assert.ThrowsAsync<BankIDException>(async () => await _sut.BeginAuthRequest(payload));
        }

        [TestCase("19900115_3874")]
        [TestCase("19900115 3874 ")]
        [TestCase("1990011-53874")]
        [TestCase(" 9001153874")]
        [TestCase("1900115 3874")]
        [TestCase("900115-3873")]
        [TestCase("9001153872")]
        [TestCase("050115-9379")]
        [TestCase("0501259370")]
        [TestCase("0513159370")]
        [TestCase("200501329370")]
        [TestCase("20050115-9375")]
        [TestCase("20050116-9370")]
        public void BeginSignRequest_ThrowsAggregateException_GivenInvalidPersonal(string personal)
        {
            // Arrange
            var payload = new SignRequest
            {
                EndUserIp = TestValidValues.EndUserIp,
                PersonalNumber = personal
            };

            // Act & Assert
            Assert.ThrowsAsync<AggregateException>(async () => {
                await _sut.BeginSignRequest(payload);
            });
        }

        [TestCase(null)]
        [TestCase("19900115-3874")]
        [TestCase("19900115 3874")]
        [TestCase("199001153874")]
        [TestCase("9001153874")]
        [TestCase("900115 3874")]
        [TestCase("900115-3874")]
        [TestCase("050115-9370")]
        [TestCase("050115 9370")]
        [TestCase("0501159370")]
        [TestCase("200501159370")]
        [TestCase("20050115-9370")]
        [TestCase("20050115-9370")]
        public void BeginSignRequest_DoesNotThrow_GivenValidPersonal(string personal)
        {
            // Arrange
            _sut = new BankIDService(_httpClient, _config, _logger);

            var payload = new SignRequest
            {
                EndUserIp = TestValidValues.EndUserIp,
                PersonalNumber = personal,
                UserVisibleData = Convert.ToBase64String(Encoding.UTF8.GetBytes("TestVisibleData")),

            };

            // Act & Assert
            Assert.DoesNotThrowAsync(async () => {
                await _sut.BeginSignRequest(payload);
            });
        }

        [Test]
        public void BeginSignRequest_Throws_BankIDException_GivenRequestForPersonalNumberAlreadyInProgress()
        {
            // Arrange
            var httpClient = FakeHttpClientFactory
                .Build("Fake400FailedAuthResponsePnAlreadyInProgress", HttpStatusCode.BadRequest);

            _sut = new BankIDService(httpClient, _config, _logger);

            var payload = new SignRequest
            {
                EndUserIp = TestValidValues.EndUserIp,
                UserVisibleData = Convert.ToBase64String(Encoding.UTF8.GetBytes("TestVisibleData")),
            };

            // Act & Assert
            Assert.ThrowsAsync<BankIDException>(async () => await _sut.BeginSignRequest(payload));
        }

        [Test]
        public void CancelRequest_Throws_AggregateException_GivenRequestWithoutOrderRef()
        {
            // Arrange
            _sut = new BankIDService(_httpClient, _config, _logger);

            var payload = new CancelAuthRequest
            {
                OrderRef = null
            };

            // Act & Assert
            Assert.ThrowsAsync<AggregateException>(async () => await _sut.CancelRequest(payload));
        }

        [Test]
        public void CollectRequestStatus_Throws_AggregateException_GivenRequestWithoutOrderRef()
        {
            // Arrange
            _sut = new BankIDService(_httpClient, _config, _logger);


            var payload = new CollectAuthRequest
            {
                OrderRef = null
            };

            // Act & Assert
            Assert.ThrowsAsync<AggregateException>(async () => await _sut.CollectRequestStatus(payload));
        }
    }
}

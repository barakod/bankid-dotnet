﻿using Barakod.Authentication.BankID.Extensions;
using Barakod.Authentication.BankID.Models.Dtos;
using Barakod.Authentication.BankID.Models.ValueObjects;
using Barakod.Authentication.BankID.Test.Constants;
using NUnit.Framework;

namespace Barakod.Authentication.BankID.Test
{
    public class AuthRequestTests
    {
        public static object[] AuthRequests =
        {
            new object[] 
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    PersonalNumber = TestValidValues.PersonalNumber,
                    Requirement = new Requirement
                    {
                        IssuerCn = new[] { IssuerCn.NordeaTestCASmartCard12, IssuerCn.NordeaTestCASoftCert13 },
                        AllowFingerprint = true,
                        CertificatePolicies = new[] { CertificatePolicie.TestBankIDForSomeBankIDBanks, CertificatePolicie.TestBankIDOnFile },
                        AutoStartTokenRequired = true,
                        CardReader = CardReader.Class2
                    }
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    Requirement = new Requirement
                    {
                        IssuerCn = new[] { IssuerCn.NordeaTestCASmartCard12, IssuerCn.NordeaTestCASoftCert13 },
                        AllowFingerprint = true,
                        CertificatePolicies = new[] { CertificatePolicie.TestBankIDForSomeBankIDBanks, CertificatePolicie.TestBankIDOnFile },
                        AutoStartTokenRequired = true,
                        CardReader = CardReader.Class2
                    }
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    PersonalNumber = TestValidValues.PersonalNumber,
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    Requirement = new Requirement
                    {
                        AllowFingerprint = true,
                        CertificatePolicies = new[] { CertificatePolicie.TestBankIDForSomeBankIDBanks, CertificatePolicie.TestBankIDOnFile },
                        AutoStartTokenRequired = true,
                        CardReader = CardReader.Class2
                    }
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    PersonalNumber = TestValidValues.PersonalNumber,
                    Requirement = new Requirement
                    {
                        CertificatePolicies = new[] { CertificatePolicie.TestBankIDOnFile, CertificatePolicie.ProdBankIDOnFile },
                        AutoStartTokenRequired = true,
                    }
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    PersonalNumber = TestValidValues.PersonalNumber,
                    Requirement = new Requirement
                    {
                        AutoStartTokenRequired = true,
                    }
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    PersonalNumber = TestValidValues.PersonalNumber,
                    Requirement = new Requirement
                    {
                        IssuerCn = new[] { IssuerCn.NordeaTestCASmartCard12, IssuerCn.NordeaTestCASoftCert13 },
                        AllowFingerprint = false,
                        CertificatePolicies = new[] { CertificatePolicie.TestBankIDForSomeBankIDBanks, CertificatePolicie.TestBankIDOnFile },
                        CardReader = CardReader.Class1
                    }
                }
            },
            new object[]
            { new AuthRequest
                {
                    EndUserIp = TestValidValues.EndUserIp,
                    PersonalNumber = TestValidValues.PersonalNumber,
                    Requirement = new Requirement
                    {
                        IssuerCn = new[] { IssuerCn.NordeaTestCASmartCard12, IssuerCn.NordeaTestCASoftCert13 },
                        AllowFingerprint = true,
                        CertificatePolicies = new[] { CertificatePolicie.TestBankIDForSomeBankIDBanks, CertificatePolicie.TestBankIDOnFile },
                        AutoStartTokenRequired = false,
                    }
                }
            }
        };

        [TestCaseSource(nameof(AuthRequests))]
        public void BeginAuthRequest_ParsesToJsonStringWithoutException(AuthRequest sut)
        {
            // Arrange, Act Assert
            Assert.DoesNotThrow(() => {
                var str = sut.TryObjectToJsonString();
            });
        }
    }
}

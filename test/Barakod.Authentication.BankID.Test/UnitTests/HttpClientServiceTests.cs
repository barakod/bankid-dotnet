﻿using Barakod.Authentication.BankID.Constants;
using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Models;
using Barakod.Authentication.BankID.Services;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Net.Http;

namespace Barakod.Authentication.BankID.Test
{
    public class HttpClientServiceTests
    {

        public static object[] InvalidHttpClients =
        {
            new object[] { new HttpClient() },
            new object[] { new HttpClient { BaseAddress = new Uri("http://xx.com") } },
            new object[] { new HttpClient { BaseAddress = new Uri("https://appapi2.foo.test.bankid.com/rp/v5") } },
            new object[] { new HttpClient { BaseAddress = new Uri("https://appapi2.foo.bankid.com/rp/v5") } },
            new object[] { new HttpClient { BaseAddress = new Uri("http://appapi2.bankid.com/rp/v5") } },
            new object[] { new HttpClient { BaseAddress = new Uri("http://appapi2.test.bankid.com/rp/v5") } },
            new object[] { new HttpClient { BaseAddress = new Uri("https://appapi2.test.bankid.com") } },
            new object[] { new HttpClient { BaseAddress = new Uri("https://appapi2.test.bankid.com") } },
            new object[] { default },
        };

        [TestCaseSource(nameof(InvalidHttpClients))]
        public void Constructor_TrowsAggregateException_GivenConfigWithInvalidHttpClient(HttpClient client)
        {
            // Arrange
            var httpClient = client;

            // Act & Assert
            Assert.Throws<AggregateException>(() => new BankIDHttpClientService(httpClient));
        }


        public static object[] ValidHttpClients =
        {
            new object[] { new HttpClient { BaseAddress = new Uri(BankIDApiBaseUrls.Test) } },
            new object[] { new HttpClient { BaseAddress = new Uri(BankIDApiBaseUrls.Production) } },
        };

        [TestCaseSource(nameof(ValidHttpClients))]
        public void Constructor_DoesNotThrow_GivenConfigWithValidHttpClient(HttpClient client)
        {
            // Arrange
            var httpClient = client;

            // Act & Assert
            Assert.DoesNotThrow(() => new BankIDHttpClientService(httpClient));
        }

        [Test]
        public void Constructor_TrowsArgumentException_GivenDefaultConfiguration()
        {
            Assert.Throws<AggregateException>(() => new BankIDHttpClientService(default(IBankIDHttpClientServiceConfiguration)));
        }
    }
}

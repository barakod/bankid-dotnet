﻿using System;
using System.Collections.Generic;
using System.IO;
using Barakod.Authentication.BankID.Extensions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID.Test.Fakes
{
    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        private HttpResponseMessage _response;

        public static HttpMessageHandler BuildFakeHttpMessageHandler(string jsonFileName, HttpStatusCode httpStatusCode)
        {
            var response = BuildResponse(jsonFileName, httpStatusCode);
            var messageHandler = new FakeHttpMessageHandler(response);

            return messageHandler;

        }

        public static HttpResponseMessage BuildResponse(string fileName, HttpStatusCode httpStatusCode)
        {
            var jsonAsBytes = typeof(FakeHttpMessageHandler)
                .ExtractEmbeddedResourceFromTypeAsByteArray($"{fileName}.json");

            var json = Encoding.UTF8.GetString(jsonAsBytes);

            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);

            streamWriter.Write(json);
            streamWriter.Flush();
            memoryStream.Position = 0;

            var httpContent = new StreamContent(memoryStream);
            return new HttpResponseMessage()
            {
                StatusCode = httpStatusCode,
                Content = httpContent
            };
        }

        public FakeHttpMessageHandler(HttpResponseMessage response)
        {
            _response = response;
        }


        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                                     CancellationToken cancellationToken)
        {
            var uri = request.Content;
            return await Task.FromResult(_response);
        }
    }
}

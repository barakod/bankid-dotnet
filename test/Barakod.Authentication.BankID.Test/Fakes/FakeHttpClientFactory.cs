﻿using Barakod.Authentication.BankID.Constants;
using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Services;
using System;
using System.Net;
using System.Net.Http;

namespace Barakod.Authentication.BankID.Test.Fakes
{
    public static class FakeHttpClientFactory
    {
        public static IBankIDHttpClientService Build(string jsonFileName,
                                               HttpStatusCode httpStatusCode,
                                               HttpClient httpClient = null,
                                               string baseUrl = BankIDApiBaseUrls.Test)
        {
            var messageHandler = FakeHttpMessageHandler.BuildFakeHttpMessageHandler(
                    jsonFileName,
                    httpStatusCode);
            var realHttpClient = httpClient ?? new HttpClient(messageHandler)
            {
                BaseAddress = new Uri(baseUrl)
            };
            var client = new BankIDHttpClientService(realHttpClient);

            return client;
        }

    }
}

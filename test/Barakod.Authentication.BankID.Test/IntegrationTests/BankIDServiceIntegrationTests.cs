﻿using Barakod.Authentication.BankID.Contracts;
using Barakod.Authentication.BankID.Exceptions;
using Barakod.Authentication.BankID.Models;
using Barakod.Authentication.BankID.Models.Dtos;
using Barakod.Authentication.BankID.Models.ValueObjects;
using Barakod.Authentication.BankID.Services;
using FakeItEasy;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Barakod.Authentication.BankID.Test
{
    class BankIDServiceIntegrationTests
    {
        private IBankIDService _sut;
        private IBankIDServiceConfiguration _config;
        private IBankIDHttpClientService _httpClient;
        private ILogger<BankIDService> _logger;
        private BankIDHttpClientServiceConfiguration _clientConfig;

        [SetUp]
        public void BaseSetup()
        {
            _logger = A.Fake<ILogger<BankIDService>>();

            _clientConfig = new BankIDHttpClientServiceConfiguration(BankIDApiEnvironment.TestV322);

            _httpClient = new BankIDHttpClientService(_clientConfig);

            _config = new BankIDConfiguration();

            _sut = new BankIDService(_httpClient, _config, _logger);
        }

        [Test]
        public void Constructor_RunsWithoutException_GivenValidConfig()
        {
            // Act & Assert
            Assert.DoesNotThrow(() => new BankIDService(_httpClient, _config, _logger));
        }

        [Test]
        public async Task BeginAuthRequest_CollectAuthRequest_CancelAuthRequest_RunsWithoutException()
        {
            // Arrange
            var authRequest = new AuthRequest
            {
                EndUserIp = "192.168.0.1",
                PersonalNumber = "850218-7498"
            };
                       
            // Act
            var authResult = await _sut.BeginAuthRequest(authRequest);

            // Assert
            authResult.AutoStartToken.Should().NotBeEmpty();
            authResult.OrderRef.Should().NotBeEmpty();

            // Arrange
            var collectRequest = new CollectAuthRequest
            {
                OrderRef = authResult.OrderRef
            };

            // Act
            var collectResult = await _sut.CollectRequestStatus(collectRequest);

            // Assert
            collectResult.Status.Should().Be(CollectStatus.Pending);
            collectResult.OrderRef.Should().BeEquivalentTo(authResult.OrderRef);
            collectResult.CompletionData.Should().BeNull();

            // Arrange
            // Act
            var cancelResult = await _sut.CancelRequest(new CancelAuthRequest { OrderRef = authResult.OrderRef });

            // Assert
            cancelResult.Should().BeTrue();
            Assert.ThrowsAsync<BankIDException>(async () => await _sut.CollectRequestStatus(collectRequest));

            // Arrange
            // Act
            authResult = await _sut.BeginAuthRequest(authRequest);
            authResult.AutoStartToken.Should().NotBeEmpty();
            authResult.OrderRef.Should().NotBeEmpty();
            await _sut.CancelRequest(new CancelAuthRequest { OrderRef = authResult.OrderRef });
        }
    }
}

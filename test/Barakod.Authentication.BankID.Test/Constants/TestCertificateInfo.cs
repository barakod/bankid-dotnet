﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Barakod.Authentication.BankID.Test.Constants
{
    public static class TestCertificateInfo
    {
        public const string PrivateCertThumbPrint = "5ca383facf7c2e3ac7eb317eaff567da0805ffa7";
        public const string RootCertThumbPrint = "b581b136673317a3422d2e4b4f6bf3f77af34798";
    }
}

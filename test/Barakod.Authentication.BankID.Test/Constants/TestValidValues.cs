﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Barakod.Authentication.BankID.Test.Constants
{
    public static class TestValidValues
    {
        public const string PersonalNumber = "19900115-3874";
        public const string EndUserIp = "192.168.0.1";
    }
}
